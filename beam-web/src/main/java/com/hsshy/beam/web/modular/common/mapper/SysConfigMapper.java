package com.hsshy.beam.web.modular.common.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hsshy.beam.web.modular.common.entity.SysConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
/**
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-10-10 21:13:03
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {

    /**
     * 根据key，查询value
     * @param  key
     * @return
     * @author hs
     */
    String getByKey(@Param("key") String key);

    /**
     * 根据key，查询value
     * @param  key
     * @return
     * @author hs
     */
    SysConfig getConfigByKey(@Param("key") String key);

    /**
     * 根据key，更新value
     * @param  key
     * @param  value
     * @return
     * @author hs
     */
    int updateValueByKey(@Param("key") String key, @Param("value") String value);
	
}
