<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.hsshy.beam</groupId>
    <artifactId>beam-parent</artifactId>
    <version>1.0.0</version>
    <name>beam-parent</name>
    <packaging>pom</packaging> <!--重要 父级packageing为 pom-->

    <modules>
        <module>beam-admin</module><!--后台管理系统-->
        <module>beam-api</module><!--api服务-->
        <module>beam-common</module><!--通用工具类-->
        <module>beam-web</module><!--通用实体、dao、service-->
        <module>beam-quartz</module><!--定时任务模块-->
    </modules>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.2.RELEASE</version>
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <beam.common.version>1.1.0</beam.common.version>
        <beam.web.version>1.1.0</beam.web.version>
        <beam.quartz.version>1.0.0</beam.quartz.version>
        <mybatis-plus-boot-starter.version>3.4.0</mybatis-plus-boot-starter.version>
        <dynamic-datasource.version>3.2.1</dynamic-datasource.version>
        <swagger.version>2.1.4</swagger.version>
        <guava.version>23.0</guava.version>
        <fastjson.version>1.2.72</fastjson.version>
        <ehcache.core.version>2.6.11</ehcache.core.version>
        <google.zxing.version>3.2.1</google.zxing.version>
        <mysql-connector-java.version>8.0.11</mysql-connector-java.version>
        <commons-lang3.version>3.3.2</commons-lang3.version>
        <commons-io-version>2.5</commons-io-version>
        <jwt.version>0.9.0</jwt.version>
        <shiro.version>1.4.0</shiro.version>
        <ehcache.version>3.3.1</ehcache.version>
        <kaptcha.version>2.3.2</kaptcha.version>
        <easy-captcha.version>1.6.2</easy-captcha.version>
        <quartz.version>2.3.0</quartz.version>
        <mp3spi.version>1.9.5.4</mp3spi.version>
        <qiniu.version>[7.2.0, 7.2.99]</qiniu.version>
        <aliyun.oss.version>2.5.0</aliyun.oss.version>
        <qcloud.cos.version>4.4</qcloud.cos.version>
        <lombok.version>1.18.12</lombok.version>
        <hibernate.validator.version>6.0.2.Final</hibernate.validator.version>
        <dubbo.version>0.2.0</dubbo.version>
        <email.version>1.3.3</email.version>
        <disruptor.version>3.4.1</disruptor.version>
        <redisson.version>3.5.0</redisson.version>
        <easyexcel.version>2.0.3</easyexcel.version>
        <weixin-popular.version>2.8.28</weixin-popular.version>
        <bcprov-jdk15on.version>1.56</bcprov-jdk15on.version>
        <thumbnailator.version>0.4.8</thumbnailator.version>
        <pinyin4j.version>2.5.1</pinyin4j.version>
        <logstash.logback.version>6.3</logstash.logback.version>
        <okhttp3.version>3.9.1</okhttp3.version>
        <spring-boot.rocketmq.version>2.1.0</spring-boot.rocketmq.version>
        <org.mapstruct.version>1.3.1.Final</org.mapstruct.version>
        <knife4j.version>2.0.4</knife4j.version>
        <jasypt.version>3.0.2</jasypt.version>
        <hutool.version>5.3.10</hutool.version>
        <flowable.version>6.5.0</flowable.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
    </dependencies>

    <dependencyManagement>

        <dependencies>

            <!-- 自定义通用模块 -->
            <dependency>
                <groupId>com.hsshy.beam</groupId>
                <artifactId>beam-common</artifactId>
                <version>${beam.common.version}</version>
            </dependency>

            <!-- 自定义通用模块 -->
            <dependency>
                <groupId>com.hsshy.beam</groupId>
                <artifactId>beam-web</artifactId>
                <version>${beam.web.version}</version>
            </dependency>

            <!-- https://mvnrepository.com/artifact/org.flowable/flowable-spring-boot-starter -->
            <dependency>
                <groupId>org.flowable</groupId>
                <artifactId>flowable-spring-boot-starter</artifactId>
                <version>${flowable.version}</version>
            </dependency>


            <dependency>
                <groupId>com.hsshy.beam</groupId>
                <artifactId>beam-quartz</artifactId>
                <version>${beam.quartz.version}</version>
            </dependency>


            <dependency>
                <groupId>org.hibernate.validator</groupId>
                <artifactId>hibernate-validator</artifactId>
                <version>${hibernate.validator.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>





            <!-- fastjson -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>${fastjson.version}</version>
            </dependency>

            <!-- ehcache -->
            <dependency>
                <groupId>org.ehcache</groupId>
                <artifactId>ehcache</artifactId>
                <version>${ehcache.version}</version>
            </dependency>

            <dependency>
                <groupId>net.sf.ehcache</groupId>
                <artifactId>ehcache-core</artifactId>
                <version>${ehcache.core.version}</version>
            </dependency>

            <!-- 二维码 使用时需在使用的模块中引入 -->
            <dependency>
                <groupId>com.google.zxing</groupId>
                <artifactId>core</artifactId>
                <version>${google.zxing.version}</version>
            </dependency>

            <!-- 二维码 使用时需在使用的模块中引入 -->
            <dependency>
                <groupId>com.google.zxing</groupId>
                <artifactId>javase</artifactId>
                <version>${google.zxing.version}</version>
            </dependency>

            <!-- mysql连接 -->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql-connector-java.version}</version>
            </dependency>


            <!-- mybatis-plus begin -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus-boot-starter.version}</version>
            </dependency>
            <!-- mybatis-plus end -->

            <!-- 多数据源模块 -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
                <version>${dynamic-datasource.version}</version>
            </dependency>

            <!-- common-lang3 -->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons-io-version}</version>
            </dependency>

            <!-- java web token-->
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jwt.version}</version>
            </dependency>


            <!--shiro依赖-->
            <dependency>
                <groupId>org.apache.shiro</groupId>
                <artifactId>shiro-core</artifactId>
                <version>${shiro.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.shiro</groupId>
                <artifactId>shiro-spring</artifactId>
                <version>${shiro.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.shiro</groupId>
                <artifactId>shiro-ehcache</artifactId>
                <version>${shiro.version}</version>
            </dependency>


            <!-- 验证码 -->
            <dependency>
                <groupId>com.github.whvcse</groupId>
                <artifactId>easy-captcha</artifactId>
                <version>${easy-captcha.version}</version>
            </dependency>


            <!--定时任务quartz-->
            <dependency>
                <groupId>org.quartz-scheduler</groupId>
                <artifactId>quartz</artifactId>
                <version>${quartz.version}</version>
            </dependency>


            <!-- 七牛 阿里 腾讯 -->
            <dependency>
                <groupId>com.qiniu</groupId>
                <artifactId>qiniu-java-sdk</artifactId>
                <version>${qiniu.version}</version>
            </dependency>
            <dependency>
                <groupId>com.aliyun.oss</groupId>
                <artifactId>aliyun-sdk-oss</artifactId>
                <version>${aliyun.oss.version}</version>
            </dependency>
            <dependency>
                <groupId>com.qcloud</groupId>
                <artifactId>cos_api</artifactId>
                <version>${qcloud.cos.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.slf4j</groupId>
                        <artifactId>slf4j-log4j12</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <!--七牛 阿里 腾讯-->


            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>

            <!--dubbo-->
            <dependency>
                <groupId>com.alibaba.boot</groupId>
                <artifactId>dubbo-spring-boot-starter</artifactId>
                <version>${dubbo.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-email</artifactId>
                <version>${email.version}</version>
            </dependency>

            <!-- disruptor 高效队列-->
            <dependency>
                <groupId>com.lmax</groupId>
                <artifactId>disruptor</artifactId>
                <version>${disruptor.version}</version>
            </dependency>

            <dependency>
                <groupId>org.redisson</groupId>
                <artifactId>redisson</artifactId>
                <version>${redisson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>easyexcel</artifactId>
                <version>${easyexcel.version}</version>
            </dependency>

            <!-- bcprov-jdk15on -->
            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcprov-jdk15on</artifactId>
                <version>${bcprov-jdk15on.version}</version>
            </dependency>

            <dependency>
                <groupId>com.github.liyiorg</groupId>
                <artifactId>weixin-popular</artifactId>
                <version>${weixin-popular.version}</version>
            </dependency>

            <dependency>
                <groupId>net.coobird</groupId>
                <artifactId>thumbnailator</artifactId>
                <version>${thumbnailator.version}</version>
            </dependency>

            <!--汉字转拼音-->
            <dependency>
                <groupId>com.belerweb</groupId>
                <artifactId>pinyin4j</artifactId>
                <version>${pinyin4j.version}</version>
            </dependency>

            <dependency>
                <groupId>net.logstash.logback</groupId>
                <artifactId>logstash-logback-encoder</artifactId>
                <version>${logstash.logback.version}</version>
            </dependency>

            <dependency>
                <groupId>com.squareup.okhttp3</groupId>
                <artifactId>okhttp</artifactId>
                <version>${okhttp3.version}</version>
            </dependency>


            <dependency>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct</artifactId>
                <version>${org.mapstruct.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct-processor</artifactId>
                <version>${org.mapstruct.version}</version>
            </dependency>

            <!-- 集成Swagger生成Api文档的增强解决方案 -->
            <!-- https://doc.xiaominfo.com/guide/-->
            <dependency>
                <groupId>com.github.xiaoymin</groupId>
                <artifactId>knife4j-spring-boot-starter</artifactId>
                <!--在引用时请在maven中央仓库搜索最新版本号-->
                <version>${knife4j.version}</version>
            </dependency>


            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>${swagger.version}</version>
            </dependency>


            <!-- 数据库加密依赖 -->
            <dependency>
                <groupId>com.github.ulisesbocchio</groupId>
                <artifactId>jasypt-spring-boot-starter</artifactId>
                <version>${jasypt.version}</version>
            </dependency>


            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>

        </dependencies>

    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <annotationProcessorPaths>
                            <path>
                                <groupId>org.mapstruct</groupId>
                                <artifactId>mapstruct-processor</artifactId>
                                <version>${org.mapstruct.version}</version>
                            </path>
                            <path>
                                <groupId>org.projectlombok</groupId>
                                <artifactId>lombok</artifactId>
                                <!-- 版本小于 1.16.12 mapstruct没写mapping将无法覆盖字段名相同的值 -->
                                <version>${lombok.version}</version>
                            </path>
                        </annotationProcessorPaths>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>


    <!-- 将项目上传到私有仓库 -->
<!--    <distributionManagement>-->
<!--        <repository>-->
<!--            <id>maven-releases</id>-->
<!--            <name>Nexus Release Repository</name>-->
<!--            <url>http://47.105.49.228:8081/repository/maven-releases/</url>-->
<!--        </repository>-->
<!--        <snapshotRepository>-->
<!--            <id>maven-snapshots</id>-->
<!--            <name>Nexus Snapshot Repository</name>-->
<!--            <url>http://47.105.49.228:8081/repository/maven-snapshots/</url>-->
<!--        </snapshotRepository>-->
<!--    </distributionManagement>-->


</project>
