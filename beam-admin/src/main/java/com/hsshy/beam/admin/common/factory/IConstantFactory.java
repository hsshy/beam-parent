package com.hsshy.beam.admin.common.factory;
import com.hsshy.beam.admin.modular.sys.entity.Dict;

import java.util.List;
/**
 * @author hs
 */
public interface IConstantFactory {

    /**
     * 根据用户ID获取其所有角色ID
     *
     * @param userId
     * @return java.util.List<java.lang.Long>
     * @author hs
     * @date 2021/9/21
     */
    List<Long> getRoleIdsById(Long userId);

    /**
     * 通过角色id获取角色名称
     *
     * @param roleId
     * @return java.lang.String
     * @author hs
     * @date 2021/9/21
     */
    String getSingleRoleName(Long roleId);

    /**
     * 获取部门名称
     *
     * @param deptId
     * @return java.lang.String
     * @author hs
     * @date 2021/9/21
     */
    String getDeptName(Long deptId);

    /**
     * 根据父级字典code和获取字典名称
     *
     * @param pcode
     * @param code
     * @return java.lang.String
     * @author hs
     * @date 2021/9/21
     */
    String getDictsByCode(String pcode, String code);

    /**
     * 根据父级字典code和获取字典名称
     *
     * @param pcode
     * @return java.util.List<com.hsshy.beam.admin.modular.sys.entity.Dict>
     * @author hs
     * @date 2021/9/21
     */
    List<Dict> getDictListByCode(String pcode);


}
