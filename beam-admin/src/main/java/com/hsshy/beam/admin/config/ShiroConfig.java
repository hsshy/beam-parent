package com.hsshy.beam.admin.config;
import com.hsshy.beam.admin.common.filter.BeamUserFilter;
import com.hsshy.beam.admin.common.shiro.UserRealm;
import com.hsshy.beam.admin.config.properties.BeamAdminProperties;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.session.mgt.ServletContainerSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * @author hs
 */
@Configuration
public class ShiroConfig {

    @Autowired
    private BeamAdminProperties beamAdminProperties;


    /**
     * 单机环境，session交给shiro管理
     */
    @Bean("sessionManager")
    @ConditionalOnProperty(prefix = "beam", name = "cluster", havingValue = "false")
    public SessionManager sessionManager(){
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setGlobalSessionTimeout(beamAdminProperties.getSessionTimeout()*1000);
        sessionManager.setSessionValidationInterval(beamAdminProperties.getSessionTimeout() * 1000);
        sessionManager.setSessionValidationSchedulerEnabled(true);
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }

//    /**
//     * 集群环境，session交给spring-session管理
//     */
//    @Bean
//    @ConditionalOnProperty(prefix = "beam", name = "cluster", havingValue = "true")
//    public ServletContainerSessionManager sessionManager() {
//        return new ServletContainerSessionManager();
//    }

    @Bean("securityManager")
    public SecurityManager securityManager(UserRealm userRealm,SessionManager sessionManager) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        securityManager.setSessionManager(sessionManager);
        securityManager.setRememberMeManager(null);
        return securityManager;
    }


    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);
        shiroFilter.setLoginUrl("/login");
        shiroFilter.setUnauthorizedUrl("/");

        //session过期拦截
        HashMap<String, Filter> myFilters = new HashMap<>();
        myFilters.put("user", new BeamUserFilter());
        shiroFilter.setFilters(myFilters);
        /**
         *
         * anon  不需要认证
         * authc 需要认证
         * user  验证通过或RememberMe登录的都可以
         *
         * 当应用开启了rememberMe时,用户下次访问时可以是一个user,但不会是authc,因为authc是需要重新认证的
         *
         * 顺序从上到下,优先级依次降低
         *
         *
         */
        Map<String, String> filterMap = new LinkedHashMap<>();
        /*swagger 资源过滤*/
        filterMap.put("/swagger/**", "anon");
        filterMap.put("/v2/api-docs", "anon");
        filterMap.put("/doc.html", "anon");
        filterMap.put("/webjars/**", "anon");
        filterMap.put("/swagger-resources/**", "anon");
        //登陆
        filterMap.put("/login/**", "anon");
        //验证码
        filterMap.put("/captcha/**", "anon");
        //全局路径（错误或者超时）
        filterMap.put("/global/*", "anon");
        filterMap.put("/api/**", "anon");
        filterMap.put("/favicon.ico", "anon");
        filterMap.put("/**", "user");
        shiroFilter.setFilterChainDefinitionMap(filterMap);

        return shiroFilter;
    }



    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }
}
