package com.hsshy.beam.api.config;
import com.hsshy.beam.common.utils.ApiRequestMappingHandlerMapping;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
/**
 * @author hs
 */
@Configuration
public class WebMvcRegistrationsConfig  implements WebMvcRegistrations {
    @Override
    public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
        ApiRequestMappingHandlerMapping handlerMapping = new ApiRequestMappingHandlerMapping();
        return handlerMapping;
    }
}