package com.hsshy.beam.api.config;
import com.hsshy.beam.api.config.properties.BeamRestProperties;
import com.hsshy.beam.api.filter.HttpServletFilter;
import com.hsshy.beam.api.interceptors.SignInterceptor;
import com.hsshy.beam.api.interceptors.TokenInterceptor;
import com.hsshy.beam.api.sign.converter.WithSignMessageConverter;
import com.hsshy.beam.api.sign.security.DataSecurityAction;
import com.hsshy.beam.api.sign.security.impl.Base64SecurityAction;
import com.hsshy.beam.common.config.DefaultFastjsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
/**
 * @author hs
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private TokenInterceptor tokenInterceptor;

    @Autowired
    private SignInterceptor signInterceptor;

    @Autowired
    private HttpServletFilter httpServletFilter;

    @Autowired
    private BeamRestProperties beamRestProperties;

    @ConditionalOnProperty(prefix = BeamRestProperties.BEAM_REST_PREFIX, name = "auth-open", havingValue = "true", matchIfMissing = true)
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //token拦截
        registry.addInterceptor(tokenInterceptor).addPathPatterns("/**");
        //签名方式1
        if(beamRestProperties.isSignOpen()){
            registry.addInterceptor(signInterceptor).addPathPatterns("/**");
        }
    }

    @Bean
    public FilterRegistrationBean registerFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(httpServletFilter);
        registration.addUrlPatterns("/*");
        registration.setOrder(1);
        return registration;
    }


//    签名方式2
//    @Bean
//    @ConditionalOnProperty(prefix = BeamRestProperties.BEAM_REST_PREFIX, name = "sign-open", havingValue = "true", matchIfMissing = true)
//    public WithSignMessageConverter withSignMessageConverter() {
//        WithSignMessageConverter withSignMessageConverter = new WithSignMessageConverter();
//        DefaultFastjsonConfig defaultFastjsonConfig = new DefaultFastjsonConfig();
//        withSignMessageConverter.setFastJsonConfig(defaultFastjsonConfig.fastjsonConfig());
//        withSignMessageConverter.setSupportedMediaTypes(defaultFastjsonConfig.getSupportedMediaType());
//        return withSignMessageConverter;
//    }

    @Bean
    public DataSecurityAction dataSecurityAction() {
        return new Base64SecurityAction();
    }




}