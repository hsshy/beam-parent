package com.hsshy.beam.api.properties;
import com.hsshy.beam.api.factory.YamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:test.yml")
@ConfigurationProperties(prefix = "test")
@Data
public class TestProperties {
    private String title;
    private String desc;
    private List<String> rule;
    private Map<String, String> mini;


}