package com.hsshy.beam.common.aop.aspectj;
import cn.hutool.core.util.ObjectUtil;
import com.hsshy.beam.common.annotion.Escape;
import com.hsshy.beam.common.utils.ConvertUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
/**
 * @author hs
 */
@Aspect
@Component
public class EscapeAspect {

    private static final Logger logger = LoggerFactory.getLogger(EscapeAspect.class);

    @Pointcut("@annotation(com.hsshy.beam.common.annotion.Escape)")
    public void pointCut() {

    }

    /**
     * 切面入口
     *
     * @param joinPoint
     * @return void
     * @author hs
     * @date 2021/10/26
     */
    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        Escape annotation = (Escape) method.getAnnotation(Escape.class);
        if (ObjectUtil.isNotEmpty(annotation.field())) {
            for (Object param : joinPoint.getArgs()) {
                if (param == null) {
                    continue;
                }
                try {
                    for (String field : annotation.field()) {
                        String[] fieldArray = field.split("\\.");
                        Field targetField = null;
                        Class<?> targetClass = param.getClass();
                        Object targetObj = null;
                        A:
                        for (int i = 0; i < fieldArray.length; i++) {
                            String subField = fieldArray[i];
                            try {
                                targetField = targetClass.getDeclaredField(subField);
                                if (ObjectUtil.isNotEmpty(targetField)) {
                                    if(ObjectUtil.isEmpty(targetObj)){
                                        targetObj = param;
                                    }
                                    targetField.setAccessible(true);
                                    Object valueObj = targetField.get(targetObj);
                                    if (ObjectUtil.isEmpty(valueObj)) {
                                        break A;
                                    }
                                    if (i == fieldArray.length - 1) {
                                        String keyword = "";
                                        for (String symbol : annotation.symbol()) {
                                            keyword = ConvertUtil.escapeStr(symbol, valueObj.toString());
                                        }
                                        targetField.set(targetObj, keyword);
                                        break A;
                                    }
                                    targetObj = valueObj;
                                    targetClass = targetObj.getClass();
                                }
                            } catch (Exception e) {
                                logger.info("空字段不进行符号转义");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}