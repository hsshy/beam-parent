package com.hsshy.beam.common.utils;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
/**
 * @description:
 * @author: hs
 * @create: 2021-04-23 16:00:35
 **/
public class ConvertUtil {

    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    private static Logger logger = LoggerFactory.getLogger(ConvertUtil.class);

    /**
     * 匹配数字
     */
    private static String matchIntegerPattern = "([0-9]\\d*)";

    /**
     * 匹配括号内的内容
     */
    private static String matchParenthesesPattern = "(?<=\\（)(\\S+)(?=\\）)";

    /**
     * 方位图
     */
    private static String[] positionName = new String[]{"北东北", "东北", "东东北", "东", "东东南", "东南", "南东南", "南", "南西南", "西南", "西西南", "西", "西西北", "西北", "北西北", "北"};
    private static String[] positionEName = new String[]{"NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"};
    private static Float[] degreeRange = new Float[]{11.26f, 33.76f, 56.26f, 78.76f, 101.26f, 123.76f, 146.26f, 168.76f, 191.26f, 213.76f, 236.26f, 258.76f, 281.26f, 303.76f, 326.26f, 348.76f};

    /**
     * 方位英文转中文
     *
     * @param positionEname 方位英文
     * @return 方位中文
     * @author hs
     * @date 2020/10/29
     */
    public static String positionENameToCName(String positionEname) {
        for (int i = 0; i < positionEName.length; i++) {
            String positionStr = positionEName[i];
            if (positionStr.equals(positionEname)) {
                return positionName[i];
            }
        }
        return null;
    }

    /**
     * 对象转字符串
     *
     * @param o
     * @return java.lang.String
     * @author hs
     * @date 2021/4/23
     */
    public static String object2Str(Object o) {
        if (ObjectUtil.isEmpty(o)) {
            return "";
        }
        return o.toString();
    }

    /**
     * 字符串转Float
     *
     * @param str
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static Float strToFloat(String str) {
        Float value = null;
        try {
            if (StrUtil.isNotEmpty(str)) {
                value = Float.parseFloat(str);
            }
        } catch (Exception e) {
            logger.error("字符串转float类型报错:{},错误信息:{}", str, e.getMessage());
        }
        return value;
    }

    /**
     * 对象转Double
     *
     * @param o
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static Double objectToDouble(Object o) {
        Double value = null;
        try {
            if (ObjectUtil.isNotEmpty(o)) {
                value = Double.parseDouble(o.toString());
            }
        } catch (Exception e) {
            logger.error("对象转Float类型报错:{},错误信息:{}", o, e.getMessage());
        }
        return value;
    }

    /**
     * 对象转Float
     *
     * @param o
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static Float objectToFloat(Object o) {
        Float value = null;
        try {
            if (ObjectUtil.isNotEmpty(o)) {
                value = Float.parseFloat(o.toString());
            }
        } catch (Exception e) {
            logger.error("对象转Float类型报错:{},错误信息:{}", o, e.getMessage());
        }
        return value;
    }

    /**
     * 对象转字符串
     *
     * @param o
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static String objectToStr(Object o) {
        String value = null;
        try {
            if (ObjectUtil.isNotEmpty(o)) {
                value = o.toString();
            }
        } catch (Exception e) {
            logger.error("对象转字符串类型报错:{},错误信息:{}", o, e.getMessage());
        }
        return value;
    }

    /**
     * 对象转Float
     *
     * @param o
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static Float decimalToFloat(BigDecimal o) {
        Float value = null;
        try {
            if (ObjectUtil.isNotEmpty(o)) {
                value = o.floatValue();
            }
        } catch (Exception e) {
            logger.error("BigDecimal转Float类型报错:{},错误信息:{}", o, e.getMessage());
        }
        return value;
    }


    /**
     * map拼接时间转LocalDateTime
     *
     * @param data
     * @param index
     * @return java.time.LocalDate
     * @author hs
     * @date 2021/5/6
     */
    public static LocalDate strToTime(Map data, String index) {
        String[] indexArr = index.split(":");
        if (ObjectUtil.isEmpty(data.get(indexArr[0])) || ObjectUtil.isEmpty(data.get(indexArr[1])) || ObjectUtil.isEmpty(data.get(indexArr[2]))) {
            return null;
        }
        String year = data.get(indexArr[0]).toString();
        String month = data.get(indexArr[1]).toString();
        String day = data.get(indexArr[2]).toString();
        year = year.length() >= 4 ? year.substring(0, 4) : "";
        month = month.length() == 1 ? "0" + month : month.substring(0, 2);
        day = day.length() == 1 ? "0" + day : day.substring(0, 2);
        String date = year + month + day;
        LocalDate localDate = LocalDate.parse(date, dateFormatter);
        return localDate;
    }

    public static String getMonthAbbreviation(int month) {
        if (month == 1) {
            return "jan";
        } else if (month == 2) {
            return "feb";
        } else if (month == 3) {
            return "mar";
        } else if (month == 4) {
            return "apr";
        } else if (month == 5) {
            return "may";
        } else if (month == 6) {
            return "jun";
        } else if (month == 7) {
            return "jul";
        } else if (month == 8) {
            return "aug";
        } else if (month == 9) {
            return "sep";
        } else if (month == 10) {
            return "oct";
        } else if (month == 11) {
            return "nov";
        } else if (month == 12) {
            return "dec";
        }
        return "";
    }

    /**
     * 经纬度度分转度
     *
     * @param dmData
     * @return java.lang.Double
     * @author hs
     * @date 2021/6/18
     */
    public static Double dmTod(String dmData) {
        String duStr = dmData.substring(0, dmData.length() - 5);
        Double d = Double.parseDouble(duStr);
        String fenStr = dmData.substring(dmData.length() - 5);
        Double fen = Double.parseDouble(fenStr) / 1000 / 60;
        d = d + fen;
        return d;
    }

    /**
     * @param data
     * @param key
     * @return java.lang.String
     * @author hs
     * @date 2021/7/12
     */
    public static String sortMap(Map data, String key) {
        return data.get(key).toString();
    }

    /**
     * 判断value是否在 min和max里
     *
     * @param min
     * @param max
     * @param minValue
     * @param maxValue
     * @return java.lang.Boolean
     * @author hs
     * @date 2021/7/12
     */
    public static Boolean isRange(Float min, Float max, BigDecimal minValue, BigDecimal maxValue) {
        if (ObjectUtil.isEmpty(min) || ObjectUtil.isEmpty(max) || ObjectUtil.isEmpty(minValue) || ObjectUtil.isEmpty(maxValue)) {
            return false;
        }
        if (minValue.floatValue() >= min && minValue.floatValue() <= max) {
            return true;
        }
        if (maxValue.floatValue() >= min && maxValue.floatValue() <= max) {
            return true;
        }
        if (minValue.floatValue() <= min && maxValue.floatValue() >= max) {
            return true;
        }
        if (minValue.floatValue() >= min && maxValue.floatValue() <= max) {
            return true;
        }
        return false;
    }


    public static String convertWaterTempDes(BigDecimal min, BigDecimal max) {
        if (ObjectUtil.isEmpty(min) || ObjectUtil.isEmpty(max)) {
            return null;
        }
        String waterTempDes;
        if (min.compareTo(max) == 0) {
            waterTempDes = "水温" + min + "℃";
        } else {
            waterTempDes = "水温" + min + "℃" + "~" + max + "℃";
        }
        return waterTempDes;
    }


    /**
     * 匹配括号内内容
     *
     * @param content 文本内容
     * @return
     * @author hs
     * @date 2020/10/27
     */
    public static String strMatchParenthesesContent(String content) {
        String result = ReUtil.get(matchParenthesesPattern, content, 0);
        return result;
    }

    /**
     * 返回匹配出的整数
     *
     * @param content 文本内容
     * @return
     * @author hs
     * @date 2020/10/23
     */
    public static Integer strMatchInteger(String content) {
        String value = ReUtil.get(matchIntegerPattern, content, 0);
        if (ObjectUtil.isNotEmpty(value)) {
            return Integer.parseInt(value);
        }
        return null;
    }


    /**
     * 计算地球上任意两点(经纬度)距离
     *
     * @param lng1 第一点经度
     * @param lat1 第一点纬度
     * @param lng2 第二点经度
     * @param lat2 第二点纬度
     * @return 返回距离 单位：米
     */
    public static double computeDistance(double lng1, double lat1, double lng2, double lat2) {
        double a, b, R;
        // 地球半径
        R = 6378137;
        lat1 = lat1 * Math.PI / 180.0;
        lat2 = lat2 * Math.PI / 180.0;
        a = lat1 - lat2;
        b = (lng1 - lng2) * Math.PI / 180.0;
        double d;
        double sa2, sb2;
        sa2 = Math.sin(a / 2.0);
        sb2 = Math.sin(b / 2.0);
        d = 2 * R * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1) * Math.cos(lat2) * sb2 * sb2));
        return d;
    }

    /**
     * 度数转方位
     *
     * @param degree
     * @return
     * @author hs
     * @date 2020/10/29
     */
    public static String degreeToPosition(Float degree) {
        if (ObjectUtil.isEmpty(degree) || degree > 360 || degree < 0) {
            return null;
        }
        if (degree >= degreeRange[degreeRange.length - 1] || degree < degreeRange[0]) {
            return positionName[degreeRange.length - 1];
        }
        for (int i = 0; i < degreeRange.length - 1; i++) {
            if (degree >= degreeRange[i] && degree < degreeRange[i + 1]) {
                return positionName[i];
            }
        }
        return null;
    }


    /**
     * 特殊符号转义
     *
     * @param symbol
     * @param value
     * @return java.lang.String
     * @author hs
     * @date 2021/11/24
     */
    public static String escapeStr(String symbol, String value) {
        value = value.replaceAll(symbol, "\\\\" + symbol);
        return value;
    }


}
