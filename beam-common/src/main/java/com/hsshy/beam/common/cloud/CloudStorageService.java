package com.hsshy.beam.common.cloud;
import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang.StringUtils;
import java.io.InputStream;
import java.util.UUID;
/**
 * 云存储(支持七牛、阿里云、腾讯云、又拍云)
 * @author hs
 * @date 2019-03-25 14:58
 */
public abstract class CloudStorageService {
    /**
     * 云存储配置信息
     */
    CloudStorageConfig config;

    /**
     * 文件路径
     *
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    public String getPath(String prefix, String suffix) {
        //生成uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //文件路径
        String path = DateUtil.today() + "/" + uuid;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "/" + path;
        }

        return path + suffix;
    }

    /**
     * 文件上传
     *
     * @param pic 图片
     * @return 返回http地址
     */
    public abstract String uploadSuffix(String pic, String suffix) throws Exception;


    /**
     * 文件上传
     *
     * @param data   文件字节数组
     * @param suffix 后缀
     * @return 返回http地址
     */
    public abstract String uploadSuffix(byte[] data, String suffix);

    /**
     * 文件上传
     *
     * @param inputStream 字节流
     * @param suffix      后缀
     * @return 返回http地址
     */
    public abstract String uploadSuffix(InputStream inputStream, String suffix);


    public abstract void delete(String path);

}
